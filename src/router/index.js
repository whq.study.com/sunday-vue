import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: 'Dashboard',
    hidden: true,
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index')
    }]
  },

  {
    path: '/information',
    component: Layout,
    redirect: '/information/table',
    name: '信息安全数据集管理',
    meta: { title: '信息安全数据集管理', icon: 'example' },
    children: [
      {
        path: 'group',
        name: '数据集分组信息',
        component: () => import('@/views/edu/information/index'),
        meta: { title: '数据集分组信息', icon: 'table' }
      },
      {
        path: 'table',
        name: '数据集列表',
        component: () => import('@/views/edu/information/list'),
        meta: { title: '数据集列表', icon: 'table' }
      },
     
      {
        path: 'save',
        name: 'information添加数据',  
        component: () => import('@/views/edu/information/save'),
        meta: { title: '添加数据', icon: 'tree' }
      },
      {
        path: 'edit/:id',   
        name: 'InformationEdit',
        component: () => import('@/views/edu/information/save'),
        meta: { title: '修改数据', noCache: true },
        hidden: true
      }
    ]
  },

  {
    path: '/example',
    component: Layout,
    redirect: '/example/table',
    name: 'Example',
    meta: { title: '自然语言数据集', icon: 'example' },
    children: [
      {
        path: 'table',
        name: 'Table',
        component: () => import('@/views/blimp/list'),
        meta: { title: '数据集列表', icon: 'table' }
      },
      {
        path: 'file',
        name: 'File',
        component: () => import('@/views/blimp/index'),
        meta: { title: '样本信息列表', icon: 'table' }
      },
      {
        path: 'save',
        name: 'blimp添加数据',  
        component: () => import('@/views/blimp/save'),
        meta: { title: '添加数据', icon: 'tree' }
      },
      {
        path: 'edit/:id',   
        name: 'BlimpInformationEdit',
        component: () => import('@/views/blimp/save'),
        meta: { title: '修改数据', noCache: true },
        hidden: true
      }

    ]
  },

  {
    path: '/form',
    component: Layout,
    name: 'CVEExample',
    meta: { title: 'cve数据集管理', icon: 'example' },
    children: [
      {
        path: 'group',
        name: 'cve分组',
        component: () => import('@/views/infosec/cve'),
        meta: { title: '数据集分组信息', icon: 'table' }
      },
      {
        path: 'index',
        name: 'CVEForm',
        component: () => import('@/views/infosec/index'),
        meta: { title: 'cve数据集列表', icon: 'form' }
      },
      {
        path: 'save',
        name: 'infosec添加数据',  
        component: () => import('@/views/infosec/save'),
        meta: { title: '添加数据', icon: 'tree' }
      },
      {
        path: 'edit/:id',   
        name: 'InfoSecEdit',
        component: () => import('@/views/infosec/save'),
        meta: { title: '修改数据', noCache: true },
        hidden: true
      }
    ]
  },

  {
    path: '/boolq',
    component: Layout,
    name: 'BoolqExample',
    meta: { title: 'boolq数据集管理', icon: 'example' },
    children: [
      {
        path: 'boolqIndex',
        name: 'BoolqForm',
        component: () => import('@/views/boolq/index'),
        meta: { title: 'boolq数据集列表', icon: 'form' }
      },
      {
        path: 'save',
        name: 'boolq添加数据',  
        component: () => import('@/views/boolq/save'),
        meta: { title: '添加数据', icon: 'tree' }
      },
      {
        path: 'edit/:id',   
        name: 'BoolqInformationEdit',
        component: () => import('@/views/boolq/save'),
        meta: { title: '修改数据', noCache: true },
        hidden: true
      }
    ]
  },
  {
    path: '/nested',
    component: Layout,
    redirect: '/nested/menu1',
    name: 'Nested',
    meta: {
      title: 'sst数据集信息',
      icon: 'example'
    },
    children: [
      
      {
        path: 'menu2',
        component: () => import('@/views/sst/list'),
        meta: { title: 'sst数据集', icon: 'table' }
      },
      {
        path: 'save',
        component: () => import('@/views/sst/save'),
        meta: { title: '添加数据', icon: 'tree' }
      },
      {
        path: 'edit/:id',   
        name: 'SSTInformationEdit',
        component: () => import('@/views/sst/save'),
        meta: { title: '修改数据', noCache: true },
        hidden: true
      }

    ]
  },

  {
    path: '/news',
    component: Layout,
    redirect: '/news/list',
    name: 'NewsExample',
    meta: {
      title: '新闻数据集',
      icon: 'example'
    },
    children: [
      {
        path: 'table',
        name: 'Table',
        component: () => import('@/views/news/list'),
        meta: { title: '类别列表', icon: 'table' }
      },
      {
        path: 'menu2',
        component: () => import('@/views/news/index'),
        meta: { title: '新闻数据集', icon: 'table' }
      },
      {
        path: 'save',
        component: () => import('@/views/news/save'),
        meta: { title: '添加数据', icon: 'tree' }
      },
      {
        path: 'edit/:id',   
        name: 'EduNewsInformation',
        component: () => import('@/views/news/save'),
        meta: { title: '修改数据', noCache: true },
        hidden: true
      }

    ]
  },

  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
