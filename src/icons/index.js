import Vue from 'vue'
import SvgIcon from '@/components/SvgIcon' // svg组件

// register globally
Vue.component('svg-icon', SvgIcon)

// https://juejin.cn/post/7117143279086338079
const requireAll = [] 
const routeFiles = import.meta.globEager('./svg/**/*.svg')
for (const path in routeFiles) {
	requireAll.push(routeFiles[path].default)
  }

