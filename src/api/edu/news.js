import request from '@/utils/request'
export default {
    //1 信息列表（条件查询分页）
    //current当前页 limit每页记录数 informationQuery条件对象
    getInformationListPage(current,limit,informationQuery) {
        return request({
           
            url: `/news/getList/${current}/${limit}`,
            method: 'post',
            //data表示把对象转换json进行传递到接口里面
            data: informationQuery
          })
    },
    //删除信息
    deleteInformationId(id) {
        return request({
            url: `/news/delete/${id}`,
            method: 'delete'
          })
    },
    
    //根据id查询信息
    getInformationInfo(id) {
        return request({
            url: `/news/findById/${id}`,
            method: 'get'
          })
    },
    //修改信息
    updateInformationInfo(id,information) {
        return request({
            url: `/news/update/${id}`,
            method: 'put',
            data: information,
            params: {
              belongId: information['belongId']
             }
          })
    },
    //添加信息，传参数的时候先把belongId放在information
     addInformation(information) {
      console.log("infom ...", )
    return request({
     url: `/news/add`,
     method: 'post',
     data: information,
     params: {
      belongId: information['belongId']
     }
      })
   },

   dealBelongId() {
    return request({
        url: `/newsCategory/idList`,
        method: 'get'
       
      })
},
   
}