import request from '@/utils/request'
export default {
    //1 信息列表（条件查询分页）
    //current当前页 limit每页记录数 informationQuery条件对象
    getInformationListPage(current,limit,informationQuery) {
        return request({
           
            url: `/cve/infosec/getList/${current}/${limit}`,
            method: 'post',
            //data表示把对象转换json进行传递到接口里面
            data: informationQuery
          })
    },
    //删除信息
    deleteInformationId(id) {
        return request({
            url: `/cve/infosec/delete/${id}`,
            method: 'delete'
          })
    },
    //添加信息
    addInformation(information) {
        return request({
            url: `/cve/infosec/add`,
            method: 'post',
            data: information
          })
    },
    //根据id查询信息
    getInformationInfo(id) {
        return request({
            url: `/cve/infosec/findById/${id}`,
            method: 'get'
          })
    },
    //修改信息
    updateInformationInfo(id,information) {
        return request({
            url: `/cve/infosec/update/${id}`,
            method: 'put',
            data: information
          })
    }
}
