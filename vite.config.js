import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv, splitVendorChunkPlugin } from 'vite'
import legacy from '@vitejs/plugin-legacy'
import vue2 from '@vitejs/plugin-vue2'
import vueJsx from '@vitejs/plugin-vue2-jsx'
import { svgBuilder } from './src/plugins/SvgBuilder';

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  // 加载环境文件
  loadEnv(mode, process.cwd());
  // 返回vite配置
  return {
    plugins: [
      vue2(),
      vueJsx(),
      svgBuilder('./src/icons/svg/'),
      splitVendorChunkPlugin(),
      legacy({
        targets: ['ie >= 11'],
        additionalLegacyPolyfills: ['regenerator-runtime/runtime']
      })
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      },
      extensions: ['', '.js', 'jsx', '.json', '.vue', '.scss', '.css']
    },
    server: {
      port: 8888,
      fs: {
        strict: false
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          // additionalData: `@import '@/styles/variables.scss';`,
        }
      }
    },
    build: {
      chunkSizeWarningLimit: 1500
    }
  }
})
